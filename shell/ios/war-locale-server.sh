#!/usr/bin/expect
set work "/work/ModemWarface"
set zdate [exec date +%Y%m%d-%H%M%S]

#写日志的函数
proc log {msg} {
#写日志的同时将消息打印在屏幕上
        puts "$msg\n"
        send_log "$msg\n"
}

log_file "$work/log/shell/locale-server.log"
set timeout 3600


spawn ssh -i $work/shell/.mykey/id_dsa_1024_114 root@192.168.1.2
expect {Enter passphrase for key}
send "********\n"
expect "#"
send "cd /work/zsxd-server\n"
expect "#"
send "svn up\n"
expect "#"
send "./start.sh\n"
expect "#"
send "exit\n"



puts "end!!!!"
